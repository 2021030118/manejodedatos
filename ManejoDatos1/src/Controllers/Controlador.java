package Controllers;

import Models.Productos;
import Models.dbProducto;
import Views.vistaProducto;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.sql.*;
import java.sql.DriverManager;

/**
 *
 * @author Angel
 */
public class Controlador implements ActionListener {

    private Views.vistaProducto vista;
    private Models.Productos pro;
    private Models.dbProducto db;

    public Controlador(vistaProducto vista, Productos pro, dbProducto db) {
        this.vista = vista;
        this.pro = pro;
        this.db = db;

        vista.btnNuevo.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnAgregar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnActualizar.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnHabilitar.addActionListener(this);
    }

    private void iniciarVista() {
        vista.setTitle(":: Productos :");
        vista.setSize(750, 600);
        vista.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnCerrar) {
            int option = JOptionPane.showConfirmDialog(vista, "¿Deseas salir?", "Decide", JOptionPane.YES_NO_OPTION);

            if (option == JOptionPane.YES_NO_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        } else if (e.getSource() == vista.btnNuevo) {
            vista.btnCancelar.setEnabled(true);
            vista.btnAgregar.setEnabled(true);
            vista.btnBuscar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnCerrar.setEnabled(true);
            vista.btnActualizar.setEnabled(true);
            vista.btnDeshabilitar.setEnabled(true);
            vista.btnHabilitar.setEnabled(true);
            vista.btnNuevo.setEnabled(true);

            vista.txtCodigo.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtFecha.setEnabled(true);
            vista.txtPrecio.setEnabled(true);

            vista.rbActivo.setEnabled(true);
            vista.rbDesactivado.setEnabled(true);
        } else if (e.getSource() == vista.btnCancelar) {
            vista.btnNuevo.setEnabled(true);

            vista.btnCancelar.setEnabled(false);
            vista.btnAgregar.setEnabled(false);
            vista.btnBuscar.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.btnCerrar.setEnabled(false);
            vista.btnActualizar.setEnabled(false);
            vista.btnDeshabilitar.setEnabled(false);
            vista.btnHabilitar.setEnabled(false);

            vista.txtCodigo.setEnabled(false);
            vista.txtNombre.setEnabled(false);
            vista.txtFecha.setEnabled(false);
            vista.txtPrecio.setEnabled(false);

            vista.rbActivo.setEnabled(true);
            vista.rbDesactivado.setEnabled(true);

            limpiar();
        } else if (e.getSource() == vista.btnLimpiar) {
            limpiar();
        } else if (e.getSource() == vista.btnAgregar) {
            try {
                //Envió los datos a la clase Productos
                extraerDatos();
                db.insertar(pro);

                JOptionPane.showMessageDialog(vista, "SE AGREGO CORRECTAMENTE");
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error al agregar: " + ex.getMessage());
            }
        } else if (e.getSource() == vista.btnActualizar) {
            try {
                //Envió los datos a la clase Productos
                extraerDatos();
                db.actualizar(pro);

                JOptionPane.showMessageDialog(vista, "SE ACTUALIZO CORRECTAMENTE");
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error al agregar: " + ex.getMessage());
            }
        } else if (e.getSource() == vista.btnBuscar) {

        } else if (e.getSource() == vista.btnHabilitar) {
            try {
                //Envió los datos a la clase Productos
                extraerDatos();
                db.habilitar(pro);

                JOptionPane.showMessageDialog(vista, "SE HABILITO CORRECTAMENTE");
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error al agregar: " + ex.getMessage());
            }
        } else if (e.getSource() == vista.btnDeshabilitar) {
              try {
                //Envió los datos a la clase Productos
                extraerDatos();
                db.deshabilitar(pro);

                JOptionPane.showMessageDialog(vista, "SE DESHABILITO CORRECTAMENTE");
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error al agregar: " + ex.getMessage());
            }
        }
    }

    public void extraerDatos() {
        pro.setCodigo(vista.txtCodigo.getText());
        pro.setNombre(vista.txtNombre.getText());
        pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
        pro.setFecha(vista.txtFecha.getText());

        if (vista.rbActivo.isSelected() == true) {
            pro.setStatus(1);
        } else if (vista.rbDesactivado.isSelected() == true) {
            pro.setStatus(0);
        }
    }

    public void limpiar() {
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtFecha.setText("");
        vista.txtPrecio.setText("");

        vista.rbDesactivado.setSelected(true);
    }

    public static void main(String[] args) {
        vistaProducto vista = new vistaProducto(new JFrame(), true);
        Productos pro = new Productos();
        dbProducto db = new dbProducto();

        try {
            Controlador control = new Controlador(vista, pro, db);
            control.iniciarVista();

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(vista, "Surgió el siguiente error al Iniciar la vista: " + ex.getMessage());

        }

    }
}
