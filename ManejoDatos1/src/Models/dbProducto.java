/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Models;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Hp
 */
public class dbProducto extends DbManejador implements dbPersistencia {

    @Override
    public void insertar(Object objecto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objecto;

        String consulta = "";
        consulta = "INSERT INTO productos(codigo,nombre,fecha,precio,estatus) VALUES(?,?,?,?,?)";

        if (this.conectar()) {
            try {
                this.sqlConsulta = conexion.prepareStatement(consulta);

                //asignar valores a la consulta
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.setString(2, pro.getNombre());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setFloat(4, pro.getPrecio());
                this.sqlConsulta.setInt(5, pro.getStatus());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                System.err.println("surgio un error al insertar " + e.getMessage());
            }
            
        }
    }

    @Override
    public void actualizar(Object objecto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objecto;

        String consulta = "";
        consulta = "update productos set nombre = ?, precio = ?, fecha = ? where codigo =  ?";

        if (this.conectar()) {
            try {
                this.sqlConsulta = conexion.prepareStatement(consulta);

                //asignar valores a la consulta
                this.sqlConsulta.setString(1, pro.getNombre());
                this.sqlConsulta.setFloat(2, pro.getPrecio());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setString(4, pro.getCodigo());

                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                System.err.println("surgio un error al insertar " + e.getMessage());
            }
        }
    }

    @Override
    public void habilitar(Object objecto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objecto;

        String consulta = "";
        consulta = "UPDATE productos SET estatus = 1 WHERE codigo = ?";

        if (this.conectar()) {
            try {
                this.sqlConsulta = conexion.prepareStatement(consulta);

                //asignar valores a la consulta
                this.sqlConsulta.setString(1, pro.getCodigo());

                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                System.err.println("surgio un error al insertar " + e.getMessage());
            }
        }
    }

    @Override
    public void deshabilitar(Object objecto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objecto;

        String consulta = "";
        consulta = "UPDATE productos SET estatus = 0 WHERE codigo = ?";

        if (this.conectar()) {
            try {
                System.out.println("Se conecto");
                this.sqlConsulta = conexion.prepareStatement(consulta);

                //asignar valores a la consulta
                this.sqlConsulta.setString(1, pro.getCodigo());

                this.sqlConsulta.executeUpdate();
                this.desconectar();
            } catch (SQLException e) {
                System.err.println("surgio un error al insertar " + e.getMessage());
            }
        }
    }

    @Override
    public boolean isExiste(int id) throws Exception {
        boolean existe = false;

        if (this.conectar()) {
            String consulta = "";
            consulta = "select * from productos where id = ? and estatus = 0";
            this.sqlConsulta = conexion.prepareStatement(consulta);

            //asignar valores a la consulta
            this.sqlConsulta.setInt(1, id);

            this.registros = this.sqlConsulta.executeQuery();

            //Validamos si existe el id en la BD
            if (this.registros.next()) existe = true;
        }
        this.desconectar();

        return existe;
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        Productos pro;

        if (this.conectar()) {
            String consulta = "SELECT * FROM productos WHERE estatus = 0 ORDER BY codigo";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);

            this.registros = this.sqlConsulta.executeQuery();

            //Sacar Registros
            while (this.registros.next()) {
                pro = new Productos();
                pro.setIdProducto(this.registros.getInt("id"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("estatus"));
                pro.setNombre(this.registros.getString("nombre"));
                lista.add(pro);
            }
        }
        this.desconectar();
        return lista;
    }

    @Override
    public ArrayList listar(String criterio) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object buscar(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Productos pro = new Productos();

        if (this.conectar()) {
            String consulta = "";
            consulta = "select * from productos where codigo = ? and estatus = 0";
            this.sqlConsulta = conexion.prepareStatement(consulta);

            //asignar valores a la consulta
            this.sqlConsulta.setString(1, codigo);

            this.registros = this.sqlConsulta.executeQuery();

            if (this.registros.next()) {
                pro.setIdProducto(this.registros.getInt("id"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("estatus"));
                pro.setNombre(this.registros.getString("nombre"));
            }
        }
        this.desconectar();

        return pro;
    }
}
