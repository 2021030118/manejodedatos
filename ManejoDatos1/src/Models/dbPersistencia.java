/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Models;

import java.util.ArrayList;

/**
 *
 * @author Hp
 */
public interface dbPersistencia {
    public void insertar(Object objecto) throws Exception;
    public void actualizar(Object objecto) throws Exception;
    public void habilitar(Object objecto) throws Exception;
    public void deshabilitar(Object objecto) throws Exception;
    
    public boolean isExiste(int id) throws Exception;
    public ArrayList listar() throws Exception;
    public ArrayList listar(String criterio) throws Exception;
    
    public Object buscar(int id) throws Exception;
    public Object buscar(String codigo) throws Exception;
}
