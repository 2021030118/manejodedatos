package com.mycompany.ejemplo8;

public class Ejemplo8 {
    public static void main(String[] args) {
        Suma_ClaseHija objsum = new Suma_ClaseHija();
        Resta_ClaseHija objres = new Resta_ClaseHija();
        Multi_ClaseHija objMulti = new Multi_ClaseHija();
        Div_ClaseHija objDiv = new Div_ClaseHija();
        
        
        objsum.setDatos();
        objsum.Operaciones();
        objsum.getMensaje();
        
        objres.setDatos();
        objres.Operaciones();
        objres.getMensaje();
        
        objMulti.setDatos();
        objMulti.Operaciones();
        objMulti.getMensaje();
        
        objDiv.setDatos();
        objDiv.Operaciones();
        objDiv.getMensaje();
    }
}
