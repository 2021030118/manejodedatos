package com.mycompany.ejemplo8;

import java.util.Scanner;

public abstract class Operaciones_ClasePadre {
    protected int num1, num2, res;
    
    Scanner teclado = new Scanner(System.in);
    
    public void setDatos(){
        System.out.print("\nCaptura el primer número: ");
        num1 = teclado.nextInt();
        System.out.print("Captura el segundo número: ");
        num2 = teclado.nextInt();
    }
    
    public abstract void Operaciones();
    
    public int getResultado(){
        return res;
    }

    public void getMensaje(){
        System.out.println("El Resultado fue: "+getResultado());
    }
}
