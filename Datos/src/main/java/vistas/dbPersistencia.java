package vistas;

import java.util.ArrayList;

/**
 *
 * @author Angel
 */
public interface dbPersistencia {
    public void insertar(Object objeto) throws Exception;
    public void actulizar (Object objeto) throws Exception;
    public void habilitar (Object objeto) throws Exception;
    public void deshabilitar (Object objeto) throws Exception;
    
    public boolean isExiste(int id) throws Exception;
    public ArrayList listar() throws Exception;
    public ArrayList listar(String criterio) throws Exception;
    
    public Object buscar(int id) throws Exception;
    public Object buscar(String codigo) throws Exception;
    
    
}
