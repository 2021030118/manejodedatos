package Modelos;

import java.sql.Date;

/**
 *
 * @author Angel
 */
public class Test {

    public static void main(String[] args) {
        Productos pro = new Productos();
        dbProducto db = new dbProducto();
        
        pro.setCodigo("0011010");
        pro.setNombre("Harina de maíz");
        pro.setPrecio(32.50f);
        pro.setFecha(new Date(2023,06,16));
        pro.setStatus(0);
        
        try{
            db.insertar(pro);
            System.out.println("Se agrego con exito el producto");
        }catch(Exception ex){
            System.err.print("Surgió un error: "+ex.getMessage());
        }
    }
    
}
