package Modelos;

import java.sql.SQLException;
import java.util.ArrayList;
import vistas.dbPersistencia;

/**
 *
 * @author Angel
 */
public class dbProducto extends DbManejador implements dbPersistencia{

    @Override
    public void insertar(Object objeto) throws Exception {
        
        Productos pro = new Productos();
        pro = (Productos) objeto;
        
        java.sql.Date fecha = new java.sql.Date(pro.getFecha().getYear(), pro.getFecha().getMonth(), pro.getFecha().getDay());
        String consulta = "";
        
        consulta = "insert into productos(codigo, nombre, fecha, precio, status) values (?,?,?,?,?);";
        this.sqlConsulta = conexion.prepareStatement(consulta);
               
        if(this.conectar()){
            try{
                System.out.println("Se conecto");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                
                //Asignar valores a los campos
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.setString(2, pro.getNombre());
                this.sqlConsulta.setDate(3, pro.getFecha());
                this.sqlConsulta.setFloat(4, pro.getPrecio());
                this.sqlConsulta.setInt(5, pro.getStatus());
                
                this.sqlConsulta.executeUpdate();
                this.desconecar();
            }catch(SQLException ex){
                 System.err.println("Surgió un erros al insertar "+ex);
            }
        }else{
            System.err.println("No fue posible conectarse a la BD");
        }
        
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void actulizar(Object objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void habilitar(Object objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void deshabilitar(Object objeto) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean isExiste(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList listar() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList listar(String criterio) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object buscar(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
