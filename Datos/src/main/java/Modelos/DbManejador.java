package Modelos;

import java.sql.*;

/**
 *
 * @author Angel
 */
public abstract class  DbManejador {
    protected Connection conexion;
    protected PreparedStatement sqlConsulta;
    protected ResultSet registro;
    
    private String usuario;
    private String database;
    private String contraseña;
    private String driver;
    private String url;

    public DbManejador(Connection conexion, PreparedStatement sqlConsulta, ResultSet registro, String usuario, String database, String contraseña, String driver, String url) {
        this.conexion = conexion;
        this.sqlConsulta = sqlConsulta;
        this.registro = registro;
        this.usuario = usuario;
        this.database = database;
        this.contraseña = contraseña;
        this.driver = driver;
        this.url = url;
        
        isDriver();
    }

    public DbManejador() {
        this.driver = "com.mysql.cj.jdbc.Driver";
        this.database = "sistemas";
        this.usuario = "root";
        this.contraseña = "";
        this.url = "jdbc:mysql://localhost:3306/sistemas";
    }

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public PreparedStatement getSqlConsulta() {
        return sqlConsulta;
    }

    public void setSqlConsulta(PreparedStatement sqlConsulta) {
        this.sqlConsulta = sqlConsulta;
    }

    public ResultSet getRegistro() {
        return registro;
    }

    public void setRegistro(ResultSet registro) {
        this.registro = registro;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }
    
     public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    public boolean isDriver(){
        boolean exito = false;
        
        try{
            Class.forName(this.getDriver());
            exito = true;
        }catch(ClassNotFoundException ex){
            exito = false;
            System.err.print("Surgio un error: "+ex.getMessage());
            System.exit(-1);
        }
        
        return   exito;
    }

    public boolean conectar(){
        boolean exito = false;
        try{
            this.setConexion(DriverManager.getConnection(this.url, this.usuario,this.contraseña));
            exito = true;
        }catch(SQLException ex){
            System.err.print("Surgio un error al conectarse: "+ex.getMessage());
            exito = false;
        }
        
        return exito;
    }
    
   public void desconecar(){
       try {
           if(!this.conexion.isClosed()) this.getConexion().close();
       } catch (SQLException ex) {
           System.err.print("No fue posible cerrar la conexión: "+ex.getMessage());
       }
    } 
    
}
