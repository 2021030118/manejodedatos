/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package Controlador;

import Vista.dlgBaseDatos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Angel
 */
public class Controlador implements ActionListener {
    private dlgBaseDatos vista;
    
    
   
    //paso 1
    private static Connection con;
    // Declaramos los datos de conexion a la bd
    private static final String driver="com.mysql.jdbc.Driver";
    private static final String user="root";
    private static final String pass="";
    private static final String url="jdbc:mysql://localhost:3306/veterinaria";
    
    public Controlador(dlgBaseDatos vista) {
        this.vista = vista;
    }
    
    // Funcion que va conectarse a mi bd de mysql
    public void conector() {
        // Reseteamos a null la conexion a la bd
        con=null;
        try{
            Class.forName(driver);
            // Nos conectamos a la bd
            con= (Connection) DriverManager.getConnection(url, user, pass);
            // Si la conexion fue exitosa mostramos un mensaje de conexion exitosa
            if (con!=null){
                vista.txtEstado.setText("Conexión Exitosa");
            }
        }
        // Si la conexion NO fue exitosa mostramos un mensaje de error
        catch (ClassNotFoundException | SQLException e){
            vista.txtEstado.setText("Conexión Fallida "+e);
        }
    }                               

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnConectar){
            conector();
        }
    }
    
   public static void main(String[] args) {
    dlgBaseDatos vista = new dlgBaseDatos(new JFrame(), true);

    Controlador controlador = new Controlador(vista);
    controlador.iniciarVista();
}
    
    
    private void iniciarVista() {
        vista.setTitle(":: Los DK´S :");
        vista.setSize(800, 800);
        vista.setVisible(true);
    }    
}

 /*private void B_mostrarActionPerformed(java.awt.event.ActionEvent evt) {                                          
       String sql="select * from productos";
       try{
           PreparedStatement pst = con.prepareStatement(sql);
           ResultSet rs = pst.executeQuery();
           DefaultTableModel model = (DefaultTableModel)this.Tabla1.getModel();
           model.setRowCount(0);
           while(rs.next()){
               model.addRow(new String[]{rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4)});
           }
       }catch(Exception ex){
           System.out.println("Error"+ ex.getMessage());
       }
       
    }         


 private void B_limipiarActionPerformed(java.awt.event.ActionEvent evt) {                                           
     DefaultTableModel model = (DefaultTableModel)this.Tabla1.getModel();
      model.setRowCount(0);
            model.setRowCount(5);
    }      */

