
package modelo;

public class Producto {
    private int codProducto, cantidadTotalProducto;
    private String descripcion, unidadMedida;
    private float precioVenta, precioCompra, ganancia;
    
     
    public Producto(int cod, int cantidad, String descripcion, String unidad, float precioVenta, float precioCompra){}
    
    public Producto(Producto pro){
        this.codProducto = pro.codProducto;
        this.cantidadTotalProducto = pro.cantidadTotalProducto;
        this.descripcion = pro.descripcion;
        this.unidadMedida = pro.unidadMedida;
        this.precioVenta = pro.precioVenta;
        this.precioCompra = pro.precioCompra;
        this.ganancia = pro.ganancia;
    }
    
     public Producto(){
        this.codProducto = 0;
        this.cantidadTotalProducto = 0;
        this.descripcion = " ";
        this.unidadMedida = " ";
        this.precioVenta = 0;
        this.precioCompra = 0;
        this.ganancia = 0;
    }
    
    public void setCodigo(int codigo){
        this.codProducto = codigo;
    }
    
    public int getCodigo(){
        return codProducto;
    }
    
    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }
    
    public String getDescripcion(){
        return descripcion;
    }
    
    public void setUnidad(String unidad){
        this.unidadMedida = unidad;
    }
    
    public String getUnidad(){
        return unidadMedida;
    }
    
    public void setPrecioCompra(float precioCompra){
       this.precioCompra = precioCompra;
    }
    
    public float getPrecioCompra(){
       return precioCompra;
    }
    
    public void setPrecioVenta(float precioVenta){
       this.precioVenta = precioVenta;
    }
    
    public float getPrecioVenta(){
       return precioVenta;
    }
     
    public void setCantidad(int cantidad){
       this.cantidadTotalProducto = cantidad;
    }
    
    public int getCantidad(){
       return cantidadTotalProducto;
    }
    
    public float calcularPrecioVenta(){
        return getPrecioVenta() * getCantidad();
    }
    
     public float calcularPrecioCompra(){
        return getPrecioCompra() * getCantidad();
    }
    
    public float calcularGanancia(){
        return calcularPrecioVenta() - calcularPrecioCompra();
    }  
}
