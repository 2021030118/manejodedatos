
package Controlador;

/**
 *
 * @author Angel
 */

import java.awt.Frame;
import modelo.Producto;
import Vista.dlgProductos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener{
    
    private Producto pro;
    private dlgProductos vista;

    public Controlador(Producto pro, dlgProductos vista) {
        this.pro = pro;
        this.vista = vista;
        
        // Hacer que el controlador ESCUCHE loa botones de la vista
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        
        vista.txtCantidad.addActionListener(this);
        vista.txtCodigo.addActionListener(this);
        vista.txtDescripcion.addActionListener(this);
        vista.txtPreCompra.addActionListener(this);
        vista.txtPreVenta.addActionListener(this);
        vista.txtUnidad.addActionListener(this);  
    }

    private void iniciarVista(){
        vista.setTitle(":: Producto ::");
        vista.setSize(500,600);
        vista.setVisible(true);
        
        vista.btnGuardar.setEnabled(true);
        vista.btnMostrar.setEnabled(true);

        vista.btnLimpiar.setEnabled(true);
        vista.btnCancelar.setEnabled(true);
        vista.btnCerrar.setEnabled(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        
        if(e.getSource()==vista.btnLimpiar){
            //Codificar el botón de limpiar 
            vista.txtCantidad.setText("");
            vista.txtCodigo.setText("");
            vista.txtDescripcion.setText("");
            vista.txtPreCompra.setText("");
            vista.txtPreVenta.setText("");
            vista.txtTotalCompra.setText("");
            vista.txtTotalVenta.setText("");
            vista.txtUnidad.setText("");
            vista.txtTotalGanancia.setText("");
            
            System.out.println("Holaaaa");
        }
        
        if(e.getSource()==vista.btnNuevo){
            //Activamos los botones y cajas de texto
            vista.btnGuardar.setEnabled(true);
            vista.btnMostrar.setEnabled(true);

            vista.btnLimpiar.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
            vista.btnCerrar.setEnabled(true);
        }
    }
    
    public static void main(String[] args) {
        
        Producto pro = new Producto();
        dlgProductos vista = new dlgProductos(new JFrame(), true);
        
        Controlador contra = new Controlador(pro, vista);
        contra.iniciarVista();
    }
}
