package Controlador;

import Vista.dlgMysql;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.HeadlessException;
import java.awt.Image;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;



public class Controlador implements ActionListener {

    private dlgMysql vista;
    private static String auxDato = " ";
    private static String dato2 = "";
    private static String dato = " ";
    private static final String driver = "com.mysql.cj.jdbc.Driver";
    private static final String user = "root";
    private static final String pass = "";
    private static final String url = "jdbc:mysql://localhost:3306/farmacia";
    private static Connection cx;

    public Controlador(dlgMysql vista) {
        this.vista = vista;
        Controlador2 contr2 = new Controlador2(vista);
        vista.btnConectar.addActionListener(this);
        vista.btnMostarDB.addActionListener(this);
        vista.btnMostarTablas.addActionListener(this);
        vista.btnMostarContTablas.addActionListener(this);
        vista.btnCrearUser.addActionListener(this);
        vista.btnMostarUser.addActionListener(this);
        vista.btnBorrarUser.addActionListener(this);
        vista.btnDesconectar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCrearBD.addActionListener(this);
        vista.btnBorrarBD.addActionListener(this);
        vista.btnConsulta.addActionListener(this);
        vista.btnEjecConsulta.addActionListener(this);
        vista.btnDarPrivi.addActionListener(this);
        vista.btnQuitPrivi.addActionListener(this);
        
       
        
        

    }

    private void iniciarVista() {
        vista.setTitle(":: MYSQL :");
        vista.setSize(1305, 804);
        vista.setVisible(true);
        
        String urlImagen = "//DESKTOP-LFLSBBA/imagenes/desconexion.png";
        ImageIcon img = new ImageIcon(urlImagen);
        Icon micono = new ImageIcon(img.getImage().getScaledInstance(vista.imagen.getWidth(),vista.imagen.getHeight(),Image.SCALE_DEFAULT));
        vista.imagen.setIcon(micono);
        
    }
    public class Controlador2 implements MouseListener{
        dlgMysql vista;

        public Controlador2(dlgMysql vista) {
            this.vista = vista;
            
            vista.TablaShow.addMouseListener(this);
            
        }

        
        @Override
        public void mouseClicked(MouseEvent e) {
            
            if (e.getSource() == vista.TablaShow) {
                try {
                    int fila = vista.TablaShow.getSelectedRow();
                    int columna = vista.TablaShow.getSelectedColumn();
                    Object valor = vista.TablaShow.getValueAt(fila, columna);
                    dato = (valor != null) ? valor.toString() : "";

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
                }
            }
            else if (e.getSource() == vista.TablesUsers) {
                 try {
                    int fila = vista.TablesUsers.getSelectedRow();
                    int columna = vista.TablesUsers.getSelectedColumn();
                    Object valor = vista.TablesUsers.getValueAt(fila, columna);
                    dato2 = (valor != null) ? valor.toString() : "";

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
                }    
             }
        }

        @Override
        public void mousePressed(MouseEvent e) {            
        }
        @Override
        public void mouseReleased(MouseEvent e) {            
        }
        @Override
        public void mouseEntered(MouseEvent e) {            
        }
        @Override
        public void mouseExited(MouseEvent e) {           
        }
    
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnConectar) {
            cx = null;
            try {
                Class.forName(driver);
                cx = (Connection) DriverManager.getConnection(url, vista.txtLoginUser.getText(), vista.txtLoginContra.getText());
                if (cx != null) {
                    JOptionPane.showMessageDialog(vista, "Conexion Establecida");
                    
                    String urlImagen = "//DESKTOP-LFLSBBA/imagenes/conexion.png";
                    ImageIcon img = new ImageIcon(urlImagen);
                    Icon micono = new ImageIcon(img.getImage().getScaledInstance(vista.imagen.getWidth(),vista.imagen.getHeight(),Image.SCALE_DEFAULT));
                    vista.imagen.setIcon(micono);
                
                }
                vista.btnBorrarUser.setEnabled(true);
                vista.btnConectar.setEnabled(true);
                vista.btnCrearUser.setEnabled(true);
                vista.btnDesconectar.setEnabled(true);
                vista.btnLimpiar.setEnabled(true);
                vista.btnMostarDB.setEnabled(true);
                vista.btnMostarUser.setEnabled(true);
                vista.txtUser.setEnabled(true);
                vista.TablaShow.setEnabled(true);
                vista.TablesUsers.setEnabled(true);
                vista.btnBorrarBD.setEnabled(true);
                vista.txtBorrarBD.setEnabled(true);
                vista.btnCrearBD.setEnabled(true);
                vista.txtCrearBD.setEnabled(true);
                vista.txtConsulta.setEnabled(true);
                vista.btnConsulta.setEnabled(true);
                vista.btnEjecConsulta.setEnabled(true);
            } catch (ClassNotFoundException | SQLException ex) {
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
            }
            
            
            
           
            

        } 
/*#####################################################################################################################*/
        else if (e.getSource() == vista.btnCrearBD) {
            String BD = vista.txtCrearBD.getText();
            String sql = "create database " + vista.txtCrearBD.getText();
            try {
                PreparedStatement pst = cx.prepareStatement(sql);
                int rowsAffected = pst.executeUpdate();

                JOptionPane.showMessageDialog(vista, "Base de Datos creada");

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
            }
            
        }
 /*#####################################################################################################################*/
        else if (e.getSource() == vista.btnBorrarBD) {         
            String BD = vista.txtBorrarBD.getText();
            String sql = "drop database " + vista.txtBorrarBD.getText();
            if (JOptionPane.showConfirmDialog(vista, "Esta seguro que desea BORRAR la BASE DE DATOS: " + BD, "Borrar", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                JOptionPane.showMessageDialog(vista, "Base de Datos no borrada");
            } 
            else {
                if (JOptionPane.showConfirmDialog(vista, "Si preciona 'SI', se BORRARA la BASE DE DATOS: " + BD + " de forma PERMANENTE, esta seguro?", "Borrar", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
                    
                }
                else{
                    try {
                        PreparedStatement pst = cx.prepareStatement(sql);
                        pst.executeUpdate();
                        JOptionPane.showMessageDialog(vista, "Base de Datos borrada");

                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
                    }
                }

            }
            vista.txtBorrarBD.setText("");
        }
/*#####################################################################################################################*/        
        else if (e.getSource() == vista.btnMostarDB) {
            
            String sql = "show databases";
            try {
                PreparedStatement pst = cx.prepareStatement(sql);
                ResultSet rs = pst.executeQuery();
                DefaultTableModel model = (DefaultTableModel) vista.TablaShow.getModel();
                model.setRowCount(0);
                while (rs.next()) {
                    model.addRow(new String[]{rs.getString(1)});
                }
                vista.TablaShow.getColumnModel().getColumn(0).setHeaderValue("Base de Datos");
                vista.TablaShow.getColumnModel().getColumn(0).setPreferredWidth(200); 
                vista.TablaShow.getTableHeader().repaint();
                vista.btnMostarTablas.setEnabled(true);
                vista.btnMostarContTablas.setEnabled(false);
                
            } 
            catch (Exception ex) {
                System.out.println("Error" + ex.getMessage());
            }
            
            
        }
        
        
/*#####################################################################################################################*/          
        else if (e.getSource() == vista.btnMostarTablas) {
            auxDato = dato;
            String sql = "show tables from " + dato;
            try {
                PreparedStatement pst = cx.prepareStatement(sql);
                ResultSet rs = pst.executeQuery();
                DefaultTableModel model = (DefaultTableModel) vista.TablaShow.getModel();
                model.setRowCount(0);
                while (rs.next()) {
                    model.addRow(new String[]{rs.getString(1)});
                }
                vista.TablaShow.getColumnModel().getColumn(0).setHeaderValue("Tablas");
                vista.TablaShow.getColumnModel().getColumn(0).setPreferredWidth(200); 
                vista.TablaShow.getTableHeader().repaint();
                vista.btnMostarTablas.setEnabled(false);
                vista.btnMostarContTablas.setEnabled(true);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
            }

            

        }
/*#####################################################################################################################*/        
        else if (e.getSource() == vista.btnMostarContTablas) {       
            String sql = "SELECT * FROM "+auxDato+"."+dato;
            try {
                PreparedStatement pst = cx.prepareStatement(sql);
                ResultSet rs = pst.executeQuery();
                ResultSetMetaData metaDatos = rs.getMetaData();
                int numeroColumnas = metaDatos.getColumnCount();
                Object[] nombreCampos = new Object[numeroColumnas];
                for (int i = 0; i < numeroColumnas; i++) {
                    nombreCampos[i] = metaDatos.getColumnLabel(i+1);
                 }
                DefaultTableModel model = (DefaultTableModel)vista.TablaShow.getModel();
                model.setColumnIdentifiers(nombreCampos);
                model.setRowCount(0);
                while (rs.next()) {

                    Object[] fila = new Object[numeroColumnas];
                    String[] info = new String[numeroColumnas];

                    //DefaultTableModel modelo=(DefaultTableModel) this.Tabla_emple.getModel();
                    // this.Tabla_emple.setModel(modelo);
                    for (int j = 1; j <= numeroColumnas; j++) {
                        info[j - 1] = rs.getString(j);
                        //fila[j] = rs.getObject(j+1);
                        //model.addRow(fila);
                    }
                    model.addRow(info);
                    vista.TablaShow.setModel(model);
                }
                vista.TablaShow.getColumnModel().getColumn(0).setHeaderValue(dato);
                vista.TablaShow.getColumnModel().getColumn(0).setPreferredWidth(200); 
                vista.TablaShow.getTableHeader().repaint();
                vista.btnMostarContTablas.setEnabled(false);
            } catch (Exception ex) {
                 System.out.println("SQLException: " + ex.getMessage());
            }
            

        }
 /*#####################################################################################################################*/          
        else if (e.getSource() == vista.btnDarPrivi) {
            String sql = "GRANT SELECT ON "+vista.txtPrivi.getText()+"  TO " + dato2;
            try {
                PreparedStatement pst = cx.prepareStatement(sql);//ejecuta consulta
                pst.executeUpdate();
                JOptionPane.showMessageDialog(vista, vista.txtConsulta.getText()+" Ejecutada correctamente");
            } catch (SQLException ex) {
                System.out.println("Error" + ex.getMessage());
            }
            vista.txtConsulta.setText("");
        }
 /*#####################################################################################################################*/ 
        else if (e.getSource() == vista.btnQuitPrivi) {
            String sql = "GRANT SELECT ON "+vista.txtPrivi.getText()+"  TO " + dato2;
            try {
                PreparedStatement pst = cx.prepareStatement(sql);//ejecuta consulta
                pst.executeUpdate();
                JOptionPane.showMessageDialog(vista, vista.txtConsulta.getText()+" Ejecutada correctamente");
            } catch (SQLException ex) {
                System.out.println("Error" + ex.getMessage());
            }
            vista.txtConsulta.setText("");
        }
/*#####################################################################################################################*/          
        else if (e.getSource() == vista.btnCrearUser) {
            String user = vista.txtUser.getText();
            if(user == "root" || user == "pma"){
                JOptionPane.showMessageDialog(vista, "No puedes crear un usuario con este nombre");
            }
            else{
                String sql = "create user " + vista.txtUser.getText() + "@localhost identified by '123456' ";
                try {
                    PreparedStatement pst = cx.prepareStatement(sql);
                    int rowsAffected = pst.executeUpdate();

                    JOptionPane.showMessageDialog(vista, "Usuario Creado");

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
                }
            }            
        } 
/*#####################################################################################################################*/          
        else if (e.getSource() == vista.btnBorrarUser) {
            if("root" == vista.txtUser.getText() || "pma" == vista.txtUser.getText()){
                JOptionPane.showMessageDialog(vista, "No puedes Borrar este usuario");
            }
            else{
                String sql = "drop user " + vista.txtUser.getText() + "@localhost;";
                try {
                    PreparedStatement pst = cx.prepareStatement(sql);
                    int rowsAffected = pst.executeUpdate();
                    JOptionPane.showMessageDialog(vista, "Usuario: " + vista.txtUser.getText() + " eliminado");

                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
                }
            }
            
        } 
/*#####################################################################################################################*/          
        else if (e.getSource() == vista.btnMostarUser) {
            String sql = "select user from mysql.user";
            try {
                PreparedStatement pst = cx.prepareStatement(sql);
                ResultSet rs = pst.executeQuery();
                DefaultTableModel model = (DefaultTableModel) vista.TablesUsers.getModel();
                model.setRowCount(0);
                while (rs.next()) {
                    model.addRow(new String[]{rs.getString(1)});
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(vista, "Surgió el siguiente error: " + ex.getMessage());
            }
            vista.txtUser.setText("");
        }
        
        
        else if (e.getSource() == vista.btnConsulta) {
            String sql = vista.txtConsulta.getText();
            try {
                PreparedStatement pst = cx.prepareStatement(sql);//ejecuta consulta

                ResultSet rs = pst.executeQuery();//

                ResultSetMetaData metaDatos = rs.getMetaData();
                int numeroColumnas = metaDatos.getColumnCount();
                Object[] nombreCampos = new Object[numeroColumnas];
                for (int i = 0; i < numeroColumnas; i++) {
                    nombreCampos[i] = metaDatos.getColumnLabel(i + 1);
                }
                DefaultTableModel model = (DefaultTableModel) vista.TablaShow.getModel();//Tabla 1
                model.setColumnIdentifiers(nombreCampos);
                model.setRowCount(0);//Tabla 2

                while (rs.next()) {

                    Object[] fila = new Object[numeroColumnas];
                    String[] info = new String[numeroColumnas];

                    //DefaultTableModel modelo=(DefaultTableModel) this.Tabla_emple.getModel();
                    // this.Tabla_emple.setModel(modelo);
                    for (int j = 1; j <= numeroColumnas; j++) {
                        info[j - 1] = rs.getString(j);
                        //fila[j] = rs.getObject(j+1);
                        //model.addRow(fila);
                    }
                    model.addRow(info);
                    vista.TablaShow.setModel(model);
                }
            } catch (SQLException ex) {
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            }
        }
        else if (e.getSource() == vista.btnEjecConsulta) {
            String sql = vista.txtConsulta.getText();
            try {
                PreparedStatement pst = cx.prepareStatement(sql);//ejecuta consulta
                pst.executeUpdate();
                JOptionPane.showMessageDialog(vista, vista.txtConsulta.getText()+" Ejecutada correctamente");
            } catch (SQLException ex) {
                System.out.println("Error" + ex.getMessage());
            }
            vista.txtConsulta.setText("");
        }
        
/*#####################################################################################################################*/        
        else if (e.getSource() == vista.btnLimpiar) {
            Object[][] limp = new Object[0][0];
            int columnas = limp[0].length;
            System.out.println(columnas);
            
            
            
        }
/*#####################################################################################################################*/         
        else if (e.getSource() == vista.btnDesconectar) {
            try {
                cx.close();
                
                String urlImagen = "//DESKTOP-LFLSBBA/imagenes/desconexion.png";
                ImageIcon img = new ImageIcon(urlImagen);
                Icon micono = new ImageIcon(img.getImage().getScaledInstance(vista.imagen.getWidth(),vista.imagen.getHeight(),Image.SCALE_DEFAULT));
                vista.imagen.setIcon(micono);
                
                vista.btnBorrarUser.setEnabled(false);
                vista.btnCrearUser.setEnabled(false);
                vista.btnDesconectar.setEnabled(false);
                vista.btnLimpiar.setEnabled(false);
                vista.btnMostarDB.setEnabled(false);
                vista.btnMostarUser.setEnabled(false);
                vista.txtUser.setEnabled(false);
                vista.TablaShow.setEnabled(false);
                vista.TablesUsers.setEnabled(false);
                vista.btnBorrarBD.setEnabled(false);
                vista.txtBorrarBD.setEnabled(false);
                vista.btnCrearBD.setEnabled(false);
                vista.txtCrearBD.setEnabled(false);
                vista.txtConsulta.setEnabled(false);
                vista.btnConsulta.setEnabled(false);
                vista.btnEjecConsulta.setEnabled(false);
                
            } catch (SQLException ex) {
                Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(JOptionPane.showConfirmDialog(vista,"Seguro que desea desconectar?, Si se desconecta se cerrara el programa","Cerrar",JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION){
                
            }
            else{
                vista.setVisible(false);
                vista.dispose();
                System.exit(0);
            }

        }
        

    }
    public static void main(String[] args) {
        dlgMysql vista = new dlgMysql();
        Controlador contr = new Controlador(vista);
        contr.iniciarVista();

    }
}
